# py-uname

This is the python equivalent of the Linux's uname command that reports basic information about the computer's software and hardware.


## Installation

This package require the [psutil](https://pypi.org/project/psutil/) library. 

```python
pip install psutil
```

## Running the script

Customize the script so that it operates like a pre-installation check on the system environment. For example;

- check the system's ram, storage space, name of drives, folders, etc.
- create the folders, unzip the library, etc.
- document the missing requirements, location of errors logs, etc.

```python
python ./flaskr/py-uname.py
```