import os

import psutil

# print("Path at terminal when executing this file")
# print(os.getcwd() + "\n")

# print("This file path, relative to os.getcwd()")
# print(__file__ + "\n")

# print("This file full path (following symlinks)")
# full_path = os.path.realpath(__file__)
# print(full_path + "\n")

# print("This file directory and name")
# path, filename = os.path.split(full_path)
# print(path + ' --> ' + filename + "\n")

# print("This file directory only")
# print(os.path.dirname(full_path))

print()
print(psutil.cpu_times())
print(psutil.virtual_memory())
print(psutil.swap_memory())
print(psutil.disk_usage('/'))

print()
print("cpu usage: \t\t", psutil.cpu_percent(interval=1))
print("user account: \t\t", psutil.users())
